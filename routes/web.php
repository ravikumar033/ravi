<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/company', 'HomeController@company')->name('company');


Route::get('/employer', 'HomeController@employer')->name('employer');
Route::post('/employer-edit', 'HomeController@employerEdit')->name('employeredit');

Route::post('/save-employer', 'HomeController@saveEmployers')->name('save-employer');
