<script>
    $(document).ready(function()
    {




        $('#addEmployee').click(function(){
            $('#employeeModal').modal('show');
            $('#employeeForm')[0].reset();
            $('.modal-title').html("<i class='fa fa-plus'></i> Add Employee");
            $('#action').val('addEmployee');
            $('#save').val('Add');
        });


        $('#EditProfile').click(function(event){
            $('#employeeModalEdit').modal('show');
            $('#employeeForm')[0].reset();
            var empname = $(this).attr("attrId");
            alert(empname);
            $('.modal-title').html("<i class='fa fa-plus'></i> Edit Employee");
            $('#action').val('addEmployee');
            $('#save').val('Add');
        });




        $("#EditProfile-button").on('click', function()
        {
            var empId = $(this).attr("id");
            var action = 'getEmployee';
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url:"{{ url('/employer-edit') }}",
                method:"POST",
                data:{empId:empId, action:action},
                dataType:"json",
                success:function(data){
                    $('#employeeModal').modal('show');
                    $('#empId').val(data.id);
                    $('#empName').val(data.name);
                    $('#empAge').val(data.age);
                    $('#empSkills').val(data.skills);
                    $('#address').val(data.address);
                    $('#designation').val(data.designation);
                    $('.modal-title').html("<i class='fa fa-plus'></i> Edit Employee");
                    $('#action').val('updateEmployee');
                    $('#save').val('Save');
                }
            })
        });




        $("#employeeModal").on('submit','#employeeForm', function(event){
            event.preventDefault();
            $('#save').attr('disabled','disabled');
            var formData = $(this).serialize();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url:"{{ url('/save-employer') }}",
                method:"post",
                data:formData,
                success:function(data){

                    $('#employeeForm')[0].reset();
                    $('#employeeModal').modal('hide');
                    $('#save').attr('disabled', false);
                }
            })
        });




        $("#employeeList").on('click', '.delete', function(){
            var empId = $(this).attr("id");
            var action = "empDelete";
            if(confirm("Are you sure you want to delete this employee?")) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url:"{{ url('/company') }}",
                    method:"POST",
                    data:{empId:empId, action:action},
                    success:function(data) {
                        employeeData.ajax.reload();
                    }
                })
            } else {
                return false;
            }
        });
    });

    </script>
