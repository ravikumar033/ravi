<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <!-- jQuery -->

    <title>webdamn.com : Datatables Add Edit Delete with Ajax, PHP & MySQL</title>
    <script src="{{URL::asset('admin/')}}/js/jquery.dataTables.min.js"></script>
    <script src="{{URL::asset('admin/')}}/js/dataTables.bootstrap.min.js"></script>
    <link rel="stylesheet" href="{{URL::asset('admin/')}}/css/dataTables.bootstrap.min.css" />


    <link rel="icon" type="image/png" href="https://webdamn.com/wp-content/themes/v2/webdamn.png">
</head>
<body class="">
<div role="navigation" class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="" class="navbar-brand">RAVI </a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="">Home</a></li>

            </ul>

        </div><!--/.nav-collapse -->
    </div>
</div>

<div class="container" style="min-height:500px;">


    @include('front.layout.ajax')
<div class="contact">

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-10">
                        <h3 class="panel-title"></h3>
                    </div>
                    <div class="col-md-2" align="right">
                        <button type="button" name="add" id="addEmployee" class="btn btn-success btn-xs">Add Employee</button>
                    </div>
                </div>
            </div>
            <table id="employeeList" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Last name</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Company name</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($Rviewdata as $testimonial)
                    <tr>
                        <td>{{$testimonial->id}}</td>
                        <td>{{$testimonial->name}}</td>
                        <td>{{$testimonial->lname}}</td>
                        <td>{{$testimonial->email}}</td>
                        <td>{{$testimonial->phone}}</td>
                        <td>{{$testimonial->companyname}}</td>
                        <td><button id="EditProfile" attrId="{{$testimonial->id}}" attrname="{{$testimonial->name}}" attrLname="{{$testimonial->lname}}" attrEmail="{{$testimonial->email}}" attrPhone="{{$testimonial->phone}}" attrCom="{{$testimonial->companyname}}">Edit</button></td>
                        <td><button id="DeleteProfile" attrId="{{$testimonial->id}}">Delete</button></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>





        <div id="employeeModal" class="modal fade">
            <div class="modal-dialog">
                <form method="post" id="employeeForm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"><i class="fa fa-plus"></i> Edit User</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="name" class="control-label">Name</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Name" required>
                            </div>
                            <div class="form-group">
                                <label for="lname" class="control-label">Last name</label>
                                <input type="text" class="form-control" id="lname" name="lname" placeholder="Last name">
                            </div>
                            <div class="form-group">
                                <label for="phone" class="control-label">Phone number</label>
                                <input type="text" class="form-control"  id="phone" name="phone" placeholder="Phone number" required>
                            </div>
                            <div class="form-group">
                                <label for="email" class="control-label">Email address</label>
                                <input type="text" class="form-control" placeholder="Email address" id="email" name="email">
                            </div>
                            <div class="form-group">
                                <label for="company name" class="control-label">Company name</label>
                                <input type="text" class="form-control" id="compname" name="compname" placeholder="Company name">
                            </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="empId" id="empId" />
                        <input type="hidden" name="action" id="action" value="" />
                        <input type="submit" name="save" id="EmployerSave" class="btn btn-info" value="Save" />
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
            </div>
            </form>
        </div>
    </div>


        <div id="employeeModalEdit" class="modal fade">
        <div class="modal-dialog">
            <form method="post" id="employeeForm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><i class="fa fa-plus"></i> Edit User</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="name" class="control-label">Name</label>
                            <input type="text" class="form-control" value="" id="name" name="name" placeholder="Name" required>
                        </div>
                        <div class="form-group">
                            <label for="lname" class="control-label">Last name</label>
                            <input type="text" class="form-control" value="" id="lname" name="lname" placeholder="Last name">
                        </div>
                        <div class="form-group">
                            <label for="phone" class="control-label">Phone number</label>
                            <input type="text" class="form-control" value=""  id="phone" name="phone" placeholder="Phone number" required>
                        </div>
                        <div class="form-group">
                            <label for="email" class="control-label">Email address</label>
                            <input type="text" class="form-control" value="" placeholder="Email address" id="email" name="email">
                        </div>
                        <div class="form-group">
                            <label for="company name" class="control-label">Company name</label>
                            <input type="text" class="form-control" value="" id="compname" name="compname" placeholder="Company name">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="empId" id="empId" />
                        <input type="hidden" name="action" id="action" value="" />
                        <input type="submit" name="save" id="EmployerSave" class="btn btn-info" value="Update" />
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
    </div>




    </div>



    </div>
    <div class="insert-post-ads1" style="margin-top:20px;">

    </div>
    </div>
    </body></html>


