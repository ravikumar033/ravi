<?php
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Log;

if (!function_exists('getCurrentUrlPrefix')) {

    function getCurrentUrlPrefix()
    {
        $string = \Route::current()->uri();
        $prefix = \Route::current()->getAction()['prefix'];
        // return $prefix;
        if (!empty($prefix)) {
            $stringC = explode('/', $string);
            if (count($stringC) > 2)
                $urlprefix = $stringC[0] . '/' . $stringC[1];
            else
                $urlprefix = $string;
        } else {
            $urlprefix = $string;
        }
        return $urlprefix;

    }

}
?>
