<?php
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Log;
use App\Model\Menu;

if (!function_exists('getstaticdata')) {

    function getstaticdata()
    {
        $menu = Menu::get('name','linkname');
        $staticpath = \URL::asset('assets/');

        $path = ([
            "company-name"=> "Nursery Plants",
            "company-short-name"=> "plants",
            "company-full-name"=> "Nursery plants Pvt.ltd",
            "logo"=> "$staticpath/images/logo.svg",
            "logo-alt-name"=> "Nursery",
            "meta-tag"=> "Nursery plants",
            "meta-desc"=> "abs dsf d f ds f ds f dsf",
            "company-address"=> "d113/e",
            "COMPANY-ADDRESS"=> [
                "Branch-office"=> "asd sa d sa d sad",
                "Branch-office1"=> "asd sa d sa d sad",
                "Branch-office2"=> "asd sa d sa d sad",
            ],
            "COMPANY-PHONE"=> [
                "company-phone-support"=> "011-25252525",
                "company-phone-sale"=> "011-25252526",
                "company-phone-enquiry"=> "011-25252520",
            ],
            "COMPANY-EMAIL"=> [
                "company-email_id-admin"=> "info@nursery.com",
                "company-email_id-sale"=> "sales@nursery.com",
                "company-email_id-support"=> "support@nursery.com",
                "company-email_id-enquiry"=> "enquiry@nursery.com",
                "company-email_id-etc"=> "noreply@nursery.com",
            ],
            "MENU-SECTION"=>
            [
                "staticMenu"=> $menu,

            ]

        ]);


        $urlprefix = $path;
        return $urlprefix;

    }

}
?>
