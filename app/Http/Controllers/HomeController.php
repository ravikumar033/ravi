<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Employer;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        return view('welcome');
    }

    public function company()
    {

        return view('deshboard.company');
    }

    public function employer()
    {
        $Rviewdata = Employer::all();
        $title = "Employer";

        return view('deshboard.employer',compact('title','Rviewdata'));
    }

    public function saveEmployers()
    {
        $inputEmploy = \Request::all();
        $SaveInput = [];
        $SaveInput['EmploymentName'] = $inputEmploy['name'];
        $SaveInput['EmploymentLname'] = $inputEmploy['lname'];
        $SaveInput['EmploymentEmail'] = $inputEmploy['email'];
        $SaveInput['EmploymentPhone'] = $inputEmploy['phone'];
        $SaveInput['EmploymentCname'] = $inputEmploy['compname'];

        $lead = new Employer;
        $lead->name = $SaveInput['EmploymentName'];
        $lead->lname = $SaveInput['EmploymentLname'];
        $lead->phone = $SaveInput['EmploymentEmail'];
        $lead->email = $SaveInput['EmploymentPhone'];
        $lead->companyname = $SaveInput['EmploymentCname'];
        $lead->save();

    }



    public function employerEdit()
    {
        dd('edit process');
        $inputEmploy = \Request::all();
        $SaveInput = [];
        $SaveInput['EmploymentName'] = $inputEmploy['name'];
        $SaveInput['EmploymentLname'] = $inputEmploy['lname'];
        $SaveInput['EmploymentEmail'] = $inputEmploy['email'];
        $SaveInput['EmploymentPhone'] = $inputEmploy['phone'];
        $SaveInput['EmploymentCname'] = $inputEmploy['compname'];

        $lead = new Employer;
        $lead->name = $SaveInput['EmploymentName'];
        $lead->lname = $SaveInput['EmploymentLname'];
        $lead->phone = $SaveInput['EmploymentEmail'];
        $lead->email = $SaveInput['EmploymentPhone'];
        $lead->companyname = $SaveInput['EmploymentCname'];
        $lead->save();

    }


    public function EditEmployers()
    {
        dd('edit');
    }
    public function deleteEmployers()
    {
        dd('delete');
    }

}
